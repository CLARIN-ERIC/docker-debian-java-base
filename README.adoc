= Debian java base docker image
:caution-caption: ☡ CAUTION
:important-caption: ❗ IMPORTANT
:note-caption: 🛈 NOTE
:sectanchors:
:sectlinks:
:sectnumlevels: 6
:sectnums:
:source-highlighter: pygments
:tip-caption: 💡 TIP
:toc-placement: preamble
:toc:
:warning-caption: ⚠ WARNING

This repository builds a debian based container providing a open JDK runtime.


== To build and test

The build, test and release pipeline is specified in the the link:.gitlab-ci.yml[GitLab CI] config file.
This pipeline or its stages can be run by GitLab.com CI as well as by a self-hosted GitLab instance.
This is elaborated in the https://about.gitlab.com/gitlab-ci/[GitLab CI docs].
In addition, the build and test stages can be run be run locally within a bare Docker container, without a GitLab CI pipeline.

NOTE: Please issue all of the following shell statements from within the root directory of this repository.

=== Without a GitLab CI pipeline

To build locally:
[source,sh]
----
sh build.sh --local --build
----

To test locally:
[source,sh]
----
sh build.sh --local --test
----
